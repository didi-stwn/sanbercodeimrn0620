// IF - ELSE 
var nama = "Junaedi"
var peran = "Werewolf"

console.log("Soal IF-ELSE")
if (nama === '') {
    console.log("Nama harus diisi!")
}
else {
    console.log("Selamat datang di Dunia Werewolf, " + nama)
    if (peran === 'Penyihir') {
        console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
    }
    else if (peran === 'Guard') {
        console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }
    else if (peran === 'Werewolf') {
        console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
    }
    else {
        console.log("Maaf, peran yang anda masukkan salah!")
    }
}
console.log()



// SWITCH CASE
var tanggal = 21; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

var parseBulan = ''

console.log("Soal SWITCH-CASE")
switch (bulan) {
    case 1: {
        parseBulan = "Januari";
        break;
    }
    case 2: {
        parseBulan = "Februari";
        break;
    }
    case 3: {
        parseBulan = "Maret";
        break;
    }
    case 4: {
        parseBulan = "April";
        break;
    }
    case 5: {
        parseBulan = "Mei";
        break;
    }
    case 6: {
        parseBulan = "Juni";
        break;
    }
    case 7: {
        parseBulan = "Juli";
        break;
    }
    case 8: {
        parseBulan = "Agustus";
        break;
    }
    case 9: {
        parseBulan = "September";
        break;
    }
    case 10: {
        parseBulan = "Oktober";
        break;
    }
    case 11: {
        parseBulan = "November";
        break;
    }
    case 12: {
        parseBulan = "Desember";
        break;
    }
    default: {
        parseBulan = "";
    }
}

console.log(tanggal + " " + parseBulan + " " + tahun)
console.log()