// SOAL 1
console.log("---------- SOAL 1 ----------")
function arrayToObject(arr) {
    var objectArr = {
        firstName: "",
        lastName: "",
        gender: "",
        age: ""
    }
    var find_age = ""

    if (arr.length === 0) {
        console.log("")
    }
    else {
        for (let i = 0; i < arr.length; i++) {
            if (!arr[i][3]) {
                find_age = "Invalid Birth Year"
            }
            else {
                var now = new Date()
                var thisYear = now.getFullYear() // 2020 (tahun sekarang)
                var rangeYear = thisYear - parseInt(arr[i][3])
                if (rangeYear < 0) {
                    find_age = "Invalid Birth Year"
                }
                else {
                    find_age = rangeYear
                }
            }

            objectArr = {
                firstName: arr[i][0],
                lastName: arr[i][1],
                gender: arr[i][2],
                age: find_age
            }
            
            console.log(`${i + 1}. ${objectArr.firstName} ${objectArr.lastName} : `, objectArr)
        }
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""
console.log()
console.log()




// SOAL 2
console.log("---------- SOAL 2 ----------")

function shoppingTime(memberId, money) {
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else if (parseInt(money) < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }
    else {
        var objectArr = {
            memberId: memberId,
            money: money,
            listPurchased: [],
            changeMoney: money
        }

        if (objectArr.changeMoney >= 1500000) {
            objectArr.listPurchased.push(`Sepatu Stacattu`)
            objectArr.changeMoney -= 1500000
        }
        if (objectArr.changeMoney >= 500000) {
            objectArr.listPurchased.push(`Baju Zoro`)
            objectArr.changeMoney -= 500000
        }
        if (objectArr.changeMoney >= 250000) {
            objectArr.listPurchased.push(`Baju H&N`)
            objectArr.changeMoney -= 250000
        }
        if (objectArr.changeMoney >= 175000) {
            objectArr.listPurchased.push(`Sweater Uniklooh`)
            objectArr.changeMoney -= 175000
        }
        if (objectArr.changeMoney >= 50000) {
            objectArr.listPurchased.push(`Casing Handphone`)
            objectArr.changeMoney -= 50000
        }

        return objectArr
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log()
console.log()







// SOAL 3
console.log("---------- SOAL 3 ----------")

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var ruteMulai = 0
    var ruteSelesai = 0
    var biaya = 0
    var output = []
    var objectArr = {
        penumpang: '',
        naikDari: '',
        tujuan: '',
        bayar: 0
    }

    if (arrPenumpang.length === 0) {
        output = []
    }
    else {
        for (let i = 0; i < arrPenumpang.length; i++) {
            for (let j = 0; j < rute.length; j++) {
                if (arrPenumpang[i][1] === rute[j]) {
                    ruteMulai = j + 1
                    break
                }
            }
            for (let j = 0; j < rute.length; j++) {
                if (arrPenumpang[i][2] === rute[j]) {
                    ruteSelesai = j + 1
                    break
                }
            }
            biaya = 2000 * (ruteSelesai - ruteMulai)

            objectArr = {
                penumpang: arrPenumpang[i][0],
                naikDari: arrPenumpang[i][1],
                tujuan: arrPenumpang[i][2],
                bayar: biaya
            }

            output.push(objectArr)
        }
    }

    return output
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]

console.log()
console.log()