import React, { Component } from 'react';
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity, TextInput, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import store from './Redux/store'

import didi from '../images/Didi_Bebas.png'
import Ariq from '../images/Ariq.jpg'

export default class About extends Component {
    openDrawer() {
        this.props.navigation.openDrawer();
    }
    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#45A7DE' }}>
                <View style={{ paddingBottom: 10, marginTop: 40, flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: 'white', borderBottomWidth: 1 }}>
                    <TouchableOpacity onPress={() => this.openDrawer()}>
                        <Icon name="align-justify" style={{ fontSize: 30, color: 'white', marginLeft: 10 }} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginRight: 10 }}>
                            <Text style={{ color: 'white' }}>Welcome,</Text>
                            <Text style={{ color: 'white' }}>{store.getState().name}</Text>
                        </View>
                        <View>
                            <Icon name="user-circle" style={{ fontSize: 30, color: 'white', marginRight: 10 }} />
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: 30, alignItems: 'center' }}>
                    <Text style={{ fontSize: 30, color: 'white' }}>Contact Me</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 30 }}>
                    <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                        <Image source={didi} style={{ height: 150, width: 150, borderRadius: 75 }} />
                        <Text style={{ textAlign: 'center', color: 'white' }}>Didi Setiawan</Text>
                        <Text style={{ textAlign: 'center', color: 'blue', fontSize: 10 }}>didi.stwn.16@gmail.com</Text>
                    </View>
                    <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                        <Image source={Ariq} style={{ height: 150, width: 150, borderRadius: 75 }} />
                        <Text style={{ textAlign: 'center', color: 'white' }}>M Ariq Alwi Masyhur</Text>
                        <Text style={{ textAlign: 'center', color: 'blue', fontSize: 10 }}>muhamadariq77@gmail.com</Text>
                    </View>
                </View>
                <View>
                    <Text style={{ color: 'white', fontSize: 20, textAlign: 'center', marginTop: 50 }}>CopyRight:</Text>
                    <View style={{ alignItems: 'center', marginTop: 10 }}>
                        <Text style={{ color: 'blue', fontSize: 15 }}> Data Global </Text>
                        <TouchableOpacity>
                            <Text onPress={() => Linking.openURL('https://api.covid19api.com')} style={{ marginTop: 0, width: 220, backgroundColor: 'white', height: 40, borderRadius: 20, fontSize: 15, textAlign: 'center', textAlignVertical: 'center', color: '#45A7DE' }}> https://api.covid19api.com </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ alignItems: 'center', marginTop: 10 }}>
                        <Text style={{ color: 'blue', fontSize: 15 }}> Data Indonesia </Text>
                        <TouchableOpacity>
                            <Text onPress={() => Linking.openURL('https://indonesia-covid-19.mathdro.id/api')} style={{ marginTop: 0, width: 300, backgroundColor: 'white', height: 40, borderRadius: 20, fontSize: 15, textAlign: 'center', textAlignVertical: 'center', color: '#45A7DE' }}> https://indonesia-covid-19.mathdro.id </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView >
        );
    }
}
