export const types = {
    CHANGE: 'CHANGE',
}

export const action = {
    change: item => {
        return { type: types.CHANGE, payload: item }
    },
}

const initialState = {
    name: ''
}

export const reducer = (state = initialState, action) => {
    const { type, payload } = action

    if (type === types.CHANGE) {
        return {
            name: payload
        }
    }
    return state
}