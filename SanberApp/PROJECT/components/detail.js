import React, { Component } from 'react';
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity, TextInput } from 'react-native';
import { Table, TableWrapper, Col, Row, Rows } from 'react-native-table-component';
import Icon from 'react-native-vector-icons/FontAwesome';
import { LineChart } from 'react-native-chart-kit';

import API from './API'

export default class Detail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataConfirm: [],
            dataRecover: [],
            dataDeath: [],
            dataLabel: [],
        }
    }
    componentDidMount() {
        let currentDate = new Date()
        currentDate.setHours(0, 0, 0, 0)
        let oneMonthAgo = new Date()
        currentDate.setHours(0, 0, 0, 0)
        oneMonthAgo.setDate(oneMonthAgo.getDate() - 30)

        let addParamsDate = `?from=${oneMonthAgo.toISOString()}&to=${currentDate.toISOString()}`

        fetch(API.getAllCasesFromDayOneByCountry + this.props.route.params.slug + addParamsDate)
            .then(response => response.json())
            .then(response => {
                let dataC = []
                let dataR = []
                let dataD = []
                let dataL = []

                for (let i = 0; i < response.length; i++) {
                    dataC.push(parseInt(this.convertNum(response[i].Confirmed)))
                    dataR.push(parseInt(this.convertNum(response[i].Recovered)))
                    dataD.push(parseInt(this.convertNum(response[i].Deaths)))
                    dataL.push(this.waktu(response[i].Date))
                }
                this.setState({
                    dataConfirm: dataC,
                    dataRecover: dataR,
                    dataDeath: dataD,
                    dataLabel: dataL,
                })
            })
            .catch(err => {
                console.log(err)
            })
    }
    convertNum(num = 0) {
        if (num != NaN) {
            return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        }
        else {
            return 0
        }
    }
    waktu(t) {
        var tahun, bulan, tanggal, jam, menit, tgl, j, m, date, d, detik;
        date = new Date(t)
        tahun = String(date.getFullYear())
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
        bulan = months[(date.getMonth())]
        tgl = date.getDate()
        if (tgl <= 9) {
            tanggal = "0" + String(tgl)
        }
        else {
            tanggal = String(tgl)
        }
        j = date.getHours()
        if (j <= 9) {
            jam = "0" + String(j)
        }
        else {
            jam = String(j)
        }
        m = date.getMinutes()
        if (m <= 9) {
            menit = "0" + String(m)
        }
        else {
            menit = String(m)
        }
        d = date.getSeconds()
        if (d <= 9) {
            detik = "0" + String(d)
        }
        else {
            detik = String(d)
        }
        return tanggal + "/" + (date.getMonth() + 1)
    }
    render() {
        const state = this.state
        // let a = 0
        return (
            <View style={{ flex: 1, backgroundColor: '#45A7DE' }}>
                <View style={{ zIndex: 1, alignItems: 'center', marginTop: 25, borderBottomColor: 'white', borderBottomWidth: 1, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: 'white', fontSize: 30 }}>{this.props.route.params.Country}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', borderBottomColor: 'white', borderBottomWidth: 1, margin: 20, marginTop: 5 }}>
                    <View style={{ backgroundColor: '#22D4DF', width: '45%', height: 'auto', borderRadius: 20, marginBottom: 5 }}>
                        <Text style={{ textAlign: 'center', color: 'white', fontSize: 20, borderBottomColor: 'white', borderBottomWidth: 1 }}>Total</Text>
                        <View style={{ alignItems: 'center', marginTop: 5 }}>
                            <Text style={{ color: 'blue' }}>Confirmed</Text>
                            <Text style={{ color: 'blue', fontSize: 20 }}>{this.convertNum(this.props.route.params.TotalConfirmed)}</Text>
                            <Text style={{ color: 'green' }}>Recovered</Text>
                            <Text style={{ color: 'green', fontSize: 20 }}>{this.convertNum(this.props.route.params.TotalRecovered)}</Text>
                            <Text style={{ color: 'red' }}>Deaths</Text>
                            <Text style={{ color: 'red', fontSize: 20 }}>{this.convertNum(this.props.route.params.TotalDeaths)}</Text>
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#22D4DF', width: '45%', marginLeft: 20, borderRadius: 20, marginBottom: 5 }}>
                        <Text style={{ textAlign: 'center', color: 'white', fontSize: 20, borderBottomColor: 'white', borderBottomWidth: 1 }}>Today</Text>
                        <View style={{ alignItems: 'center', marginTop: 5 }}>
                            <Text style={{ color: 'blue' }}>Confirmed</Text>
                            <Text style={{ color: 'blue', fontSize: 20 }}>{this.convertNum(this.props.route.params.NewConfirmed)}</Text>
                            <Text style={{ color: 'green' }}>Recovered</Text>
                            <Text style={{ color: 'green', fontSize: 20 }}>{this.convertNum(this.props.route.params.NewRecovered)}</Text>
                            <Text style={{ color: 'red' }}>Deaths</Text>
                            <Text style={{ color: 'red', fontSize: 20 }}>{this.convertNum(this.props.route.params.NewDeaths)}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: -20, alignItems: 'center', marginBottom: 10 }}>
                    <Text style={{ fontSize: 20, color: 'white' }}>Cases in 30 Days ago</Text>
                </View>
                {/* <View style={{ flex: 1, margin: 20, marginBottom: 10, marginTop: -10, backgroundColor: '#22D4DF', borderRadius: 20 }}>
                    <Table>
                        <Row data={state.dataHeader} flexArr={[3, 1, 1, 1]} textStyle={{ textAlign: 'center', color: 'white', borderBottomWidth: 1, borderBottomColor: 'white' }} />
                    </Table>
                    <ScrollView>
                        <Table>
                            <Rows data={state.dataAll} flexArr={[3, 1, 1, 1]} style={{ height: 28 }} textStyle={{ textAlign: 'center', color: 'white' }} />
                        </Table>
                    </ScrollView>
                </View> */}
                {
                    state.dataLabel.length === 0 &&
                    <View style={{ alignItems: 'center', marginTop: 50 }}>
                        <Text style={{ color: 'white' }}>Loading...</Text>
                    </View>
                }
                {state.dataLabel.length > 0 &&
                    <View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 20, marginRight: 20, marginTop: -10, marginBottom: 5 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <Icon name="stop" style={{ color: 'rgba(0, 0, 255, 1)', textAlign: 'center', textAlignVertical: 'center' }} />
                                <Text style={{ marginLeft: 5, color: 'rgba(0, 0, 255, 1)', textAlign: 'center', textAlignVertical: 'center' }}>Confirmed</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <Icon name="stop" style={{ color: 'rgba(0, 255, 0, 1)', textAlign: 'center', textAlignVertical: 'center' }} />
                                <Text style={{ marginLeft: 5, color: 'rgba(0, 255, 0, 1)', textAlign: 'center', textAlignVertical: 'center' }}>Recovered</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <Icon name="stop" style={{ color: 'rgba(255, 0, 0, 1)', textAlign: 'center', textAlignVertical: 'center' }} />
                                <Text style={{ marginLeft: 5, color: 'rgba(255, 0, 0, 1)', textAlign: 'center', textAlignVertical: 'center' }}>Deaths</Text>
                            </View>
                        </View>
                        <ScrollView horizontal={true} style={{ margin: 20, marginTop: 0 }}>
                            <LineChart
                                data={{
                                    labels: state.dataLabel,
                                    datasets: [
                                        {
                                            data: state.dataConfirm,
                                            color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`,
                                            name: "Jawa Barat",
                                            legendFontColor: "white",
                                            legendFontSize: 11
                                        },
                                        {
                                            data: state.dataRecover,
                                            color: (opacity = 1) => `rgba(0, 255, 0, ${opacity})`,
                                        },
                                        {
                                            data: state.dataDeath,
                                            color: (opacity = 1) => `rgba(255, 0, 0, ${opacity})`,
                                        }
                                    ]
                                }}
                                width={1000} // from react-native
                                height={320}
                                chartConfig={
                                    {
                                        backgroundGradientFrom: '#22D4DF',
                                        backgroundGradientTo: '#22D4DF',
                                        decimalPlaces: 0, // optional, defaults to 2dp
                                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                        propsForDots: {
                                            r: "4",
                                        }
                                    }
                                }
                                style={{
                                    borderRadius: 20,
                                    padding: 0,
                                    margin: 0
                                }}
                            />
                        </ScrollView>
                    </View>
                    }

            </View >
        );
    }
}