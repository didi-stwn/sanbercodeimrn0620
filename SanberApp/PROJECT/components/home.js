import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import store from './Redux/store'

import API from './API'

export default class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataGlobal: {},
            dataCountry: [],
            countCountry: 0
        }
    }

    componentDidMount() {
        fetch(API.summary)
            .then(response => response.json())
            .then(response => {
                this.setState({
                    dataGlobal: response.Global,
                    dataCountry: response.Countries,
                    countCountry: response.Countries.length
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    convertNum(num = 0) {
        if (num != NaN) {
            return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        }
        else {
            return 0
        }
    }

    getDetail(Country, slug, NewConfirmed, NewRecovered, NewDeaths, TotalConfirmed, TotalRecovered, TotalDeaths) {
        this.props.navigation.push("Detail", { Country, slug, NewConfirmed, NewRecovered, NewDeaths, TotalConfirmed, TotalRecovered, TotalDeaths })
    }

    openDrawer() {
        this.props.navigation.openDrawer();
    }

    render() {
        const state = this.state
        return (
            <View style={{ flex: 1, backgroundColor: '#45A7DE' }}>
                <View style={{ paddingBottom: 10, marginTop: 40, flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: 'white', borderBottomWidth: 1 }}>
                    <TouchableOpacity onPress={() => this.openDrawer()}>
                        <Icon name="align-justify" style={{ fontSize: 30, color: 'white', marginLeft: 10 }} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginRight: 10 }}>
                            <Text style={{ color: 'white' }}>Welcome,</Text>
                            <Text style={{ color: 'white' }}>{store.getState().name}</Text>
                        </View>
                        <View>
                            <Icon name="user-circle" style={{ fontSize: 30, color: 'white', marginRight: 10 }} />
                        </View>
                    </View>
                </View>
                <View style={{ alignItems: 'center', marginTop: 5, borderBottomColor: 'white', borderBottomWidth: 1, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: 'white', fontSize: 20 }}>Cases in the World</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', borderBottomColor: 'white', borderBottomWidth: 1, margin: 20, marginTop: 5 }}>
                    <View style={{ backgroundColor: '#22D4DF', width: '45%', height: 'auto', borderRadius: 20, marginBottom: 5 }}>
                        <Text style={{ textAlign: 'center', color: 'white', fontSize: 20, borderBottomColor: 'white', borderBottomWidth: 1 }}>Total</Text>
                        <View style={{ alignItems: 'center', marginTop: 5 }}>
                            <Text style={{ color: 'blue' }}>Confirmed</Text>
                            <Text style={{ color: 'blue', fontSize: 20 }}>{this.convertNum(state.dataGlobal.TotalConfirmed)}</Text>
                            <Text style={{ color: 'green' }}>Recovered</Text>
                            <Text style={{ color: 'green', fontSize: 20 }}>{this.convertNum(state.dataGlobal.TotalRecovered)}</Text>
                            <Text style={{ color: 'red' }}>Deaths</Text>
                            <Text style={{ color: 'red', fontSize: 20 }}>{this.convertNum(state.dataGlobal.TotalDeaths)}</Text>
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#22D4DF', width: '45%', marginLeft: 20, borderRadius: 20, marginBottom: 5 }}>
                        <Text style={{ textAlign: 'center', color: 'white', fontSize: 20, borderBottomColor: 'white', borderBottomWidth: 1 }}>Today</Text>
                        <View style={{ alignItems: 'center', marginTop: 5 }}>
                            <Text style={{ color: 'blue' }}>Confirmed</Text>
                            <Text style={{ color: 'blue', fontSize: 20 }}>{this.convertNum(state.dataGlobal.NewConfirmed)}</Text>
                            <Text style={{ color: 'green' }}>Recovered</Text>
                            <Text style={{ color: 'green', fontSize: 20 }}>{this.convertNum(state.dataGlobal.NewRecovered)}</Text>
                            <Text style={{ color: 'red' }}>Deaths</Text>
                            <Text style={{ color: 'red', fontSize: 20 }}>{this.convertNum(state.dataGlobal.NewDeaths)}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: -20, alignItems: 'center', marginBottom: 5 }}>
                    <Text style={{ fontSize: 20, color: 'white' }}>Today in {state.countCountry} Countries</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    {state.countCountry > 0 &&
                        <FlatList
                            data={state.dataCountry}
                            renderItem={(data) => <ListCountry data={data.item} detail={this.getDetail.bind(this)} />}
                            keyExtractor={(item) => item.CountryCode}
                            numColumns={2}
                        />
                    }
                </View>
            </View>
        )
    }
};

class ListCountry extends React.Component {

    convertNum(num = 0) {
        if (num != NaN) {
            return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        }
        else {
            return 0
        }
    };

    render() {
        const data = this.props.data
        return (
            <TouchableOpacity onPress={() => this.props.detail(data.Country, data.Slug, data.NewConfirmed, data.NewRecovered, data.NewDeaths, data.TotalConfirmed, data.TotalRecovered, data.TotalDeaths)} style={{ backgroundColor: '#22D4DF', width: '45%', marginLeft: 10, borderRadius: 10, marginBottom: 10 }}>
                <Text style={{ textAlign: 'center', color: 'white', fontSize: 15, borderBottomColor: 'white', borderBottomWidth: 1 }}>{data.Country}</Text>
                <View style={{ alignItems: 'center', marginTop: 5 }}>
                    <Text style={{ color: 'blue' }}>Confirmed</Text>
                    <Text style={{ color: 'blue', fontSize: 20 }}>{this.convertNum(data.NewConfirmed)}</Text>
                    <Text style={{ color: 'green' }}>Recovered</Text>
                    <Text style={{ color: 'green', fontSize: 20 }}>{this.convertNum(data.NewRecovered)}</Text>
                    <Text style={{ color: 'red' }}>Deaths</Text>
                    <Text style={{ color: 'red', fontSize: 20 }}>{this.convertNum(data.NewDeaths)}</Text>
                </View>
            </TouchableOpacity>
        )
    }
};