import React, { Component } from 'react';
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity, TextInput } from 'react-native';

export default class Register extends Component {
    Login() {
        this.props.navigation.navigate('Login')
    }
    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#45A7DE' }}>
                <View style={{ alignItems: 'center', borderBottomColor: 'white', borderBottomWidth: 1, margin: 70 }}>
                    <Text style={{ fontSize: 30, color: 'white' }}> REGISTER </Text>
                </View>
                <View style={{ margin: 20, marginTop: -30 }}>
                    <Text style={{ color: 'white' }}>Username</Text>
                    <TextInput
                        style={{ backgroundColor: 'white', marginTop: 8, borderRadius: 10, height: 40 }}
                        placeholder='   username...'
                        onChangeText={(searchText => this.setState({ searchText }))}
                    />

                    <Text style={{ color: 'white', marginTop: 20 }}>Email</Text>
                    <TextInput
                        style={{ backgroundColor: 'white', marginTop: 8, borderRadius: 10, height: 40 }}
                        placeholder='   email...'
                        onChangeText={(searchText => this.setState({ searchText }))}
                    />

                    <Text style={{ color: 'white', marginTop: 20 }}>Password</Text>
                    <TextInput
                        style={{ backgroundColor: 'white', marginTop: 8, borderRadius: 10, height: 40 }}
                        placeholder='   password...'
                        onChangeText={(searchText => this.setState({ searchText }))}
                    />

                    <Text style={{ color: 'white', marginTop: 20 }}>Password</Text>
                    <TextInput
                        style={{ backgroundColor: 'white', marginTop: 8, borderRadius: 10, height: 40 }}
                        placeholder='   password...'
                        onChangeText={(searchText => this.setState({ searchText }))}
                    />
                </View>
                <View style={{ justifyContent: 'center', marginTop: 10, marginBottom: 30 }}>
                    <TouchableOpacity onPress={() => this.Login()} style={{ flexDirection: 'row', justifyContent: 'center', height: 40 }}>
                        <Text style={{ width: 120, borderRadius: 20, fontSize: 20, backgroundColor: '#22D4DF', color: 'white', textAlign: 'center', textAlignVertical: 'center' }}>CONFIRM</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}