const global = "https://api.covid19api.com"
const indo = "https://indonesia-covid-19.mathdro.id"

const api = {
    getAPI: global,
    summary: global + "/summary",
    getAllCasesFromDayOneByCountry: global + "/country/", //+nama negara dari slug (getAllCountries) ? from2020-03-01T00:00:00%to=2020-04-01T00:00:00Z


    indoSummary: indo + "/api",
    indoToday: indo + "/api/harian",
    indoProvinsi: indo + "/api/provinsi",    
}

module.exports = api
