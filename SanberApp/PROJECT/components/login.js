import React from 'react';
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux'

import { action } from './Redux/todoReducer'

class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
        }
    }
    Login() {
        this.props.dispatch(action.change(this.state.username))
        this.props.navigation.navigate('Home')
    }
    Register() {
        this.props.navigation.navigate('Register')
    }
    render() {
        return ( 
            <ScrollView style={{ flex: 1, backgroundColor: '#45A7DE' }}>
                <View style={{ marginTop: 60, alignItems: 'center' }}>
                    <Icon name="map-marker" style={{ color: 'white', fontSize: 150 }} />
                </View>
                <View style={{ alignItems: 'center', marginTop: 20 }}>
                    <Text style={{ color: 'white', fontSize: 15 }}>Welcome to <Text style={{ color: 'blue', fontSize: 18 }}>CoVid-19 Tracker</Text></Text>
                    <Text style={{ color: 'white', fontSize: 15 }}><Text style={{ color: 'blue', fontSize: 18 }}>Login</Text> and <Text style={{ color: 'blue', fontSize: 18 }}>Get</Text> your <Text style={{ color: 'blue', fontSize: 18 }}>information</Text></Text>
                </View>
                <View style={{ margin: 20, marginTop: 30, }}>
                    <Text style={{ fontSize: 15, color: 'white' }}>Username</Text>
                    <TextInput
                        style={{ backgroundColor: 'white', marginTop: 8, borderRadius: 10, height: 40, color: '#45A7DE', paddingLeft: 20 }}
                        placeholder='username...'
                        onChangeText={(input => this.setState({ username: input }))}
                    />
                </View>
                <View style={{ margin: 20, marginTop: 0 }}>
                    <Text style={{ fontSize: 15, color: 'white' }}>Password</Text>
                    <TextInput
                        style={{ backgroundColor: 'white', marginTop: 8, borderRadius: 10, height: 40, color: '#45A7DE', paddingLeft: 20 }}
                        placeholder='password...'
                        onChangeText={(input => this.setState({ password: input }))}
                        secureTextEntry={true}
                    />
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 20 }}>
                    <View>
                        <Text style={{ color: 'white' }}>Belum punya akun ?</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => this.Register()}>
                            <Text style={{ color: 'blue' }}> Daftar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ justifyContent: 'center', marginTop: 10, marginBottom: 30 }}>
                    <TouchableOpacity onPress={() => this.Login()} style={{ flexDirection: 'row', justifyContent: 'center', height: 40 }}>
                        <Text style={{ width: 120, borderRadius: 20, fontSize: 20, backgroundColor: '#22D4DF', color: 'white', textAlign: 'center', textAlignVertical: 'center' }}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}
export default connect()(Login)