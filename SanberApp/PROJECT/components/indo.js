import React, { Component } from 'react';
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import API from './API'
import store from './Redux/store'

export default class Indo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataSummaryToday: [],
            dataSummaryAll: [],
            dataProv: [],
        }
    }
    componentDidMount() {
        fetch(API.indoToday)
            .then(response => response.json())
            .then(response => {
                this.setState({
                    dataSummaryToday: response.data[response.data.length - 1]
                })
                fetch(API.indoSummary)
                    .then(response => response.json())
                    .then(response => {
                        this.setState({
                            dataSummaryAll: response
                        })
                        fetch(API.indoProvinsi)
                            .then(response => response.json())
                            .then(response => {
                                this.setState({
                                    dataProv: response.data
                                })
                            })
                            .catch(err => {
                                console.log(err)
                            })
                    })
                    .catch(err => {
                        console.log(err)
                    })
            })
            .catch(err => {
                console.log(err)
            })
    }
    convertNum(num = 0) {
        if (num != null) {
            return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        }
        else {
            return 0
        }
    }
    openDrawer() {
        this.props.navigation.openDrawer();
    }
    render() {
        const state = this.state
        return (
            <View style={{ flex: 1, backgroundColor: '#45A7DE' }}>
                <View style={{ paddingBottom: 10, marginTop: 40, flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: 'white', borderBottomWidth: 1 }}>
                    <TouchableOpacity onPress={() => this.openDrawer()}>
                        <Icon name="align-justify" style={{ fontSize: 30, color: 'white', marginLeft: 10 }} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginRight: 10 }}>
                            <Text style={{ color: 'white' }}>Welcome,</Text>
                            <Text style={{ color: 'white' }}>{store.getState().name}</Text>
                        </View>
                        <View>
                            <Icon name="user-circle" style={{ fontSize: 30, color: 'white', marginRight: 10 }} />
                        </View>
                    </View>
                </View>
                <View style={{ alignItems: 'center', marginTop: 5, borderBottomColor: 'white', borderBottomWidth: 1, marginLeft: 20, marginRight: 20 }}>
                    <Text style={{ color: 'white', fontSize: 20 }}>Cases in the Indonesia</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'center', borderBottomColor: 'white', borderBottomWidth: 1, margin: 20, marginTop: 5 }}>
                    <View style={{ backgroundColor: '#22D4DF', width: '45%', height: 'auto', borderRadius: 20, marginBottom: 5 }}>
                        <Text style={{ textAlign: 'center', color: 'white', fontSize: 20, borderBottomColor: 'white', borderBottomWidth: 1 }}>Total</Text>
                        <View style={{ alignItems: 'center', marginTop: 5 }}>
                            <Text style={{ color: 'blue' }}>Confirmed</Text>
                            <Text style={{ color: 'blue', fontSize: 20 }}>{this.convertNum(state.dataSummaryAll.jumlahKasus)}</Text>
                            <Text style={{ color: 'green' }}>Recovered</Text>
                            <Text style={{ color: 'green', fontSize: 20 }}>{this.convertNum(state.dataSummaryAll.sembuh)}</Text>
                            <Text style={{ color: 'red' }}>Deaths</Text>
                            <Text style={{ color: 'red', fontSize: 20 }}>{this.convertNum(state.dataSummaryAll.meninggal)}</Text>
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#22D4DF', width: '45%', marginLeft: 20, borderRadius: 20, marginBottom: 5 }}>
                        <Text style={{ textAlign: 'center', color: 'white', fontSize: 20, borderBottomColor: 'white', borderBottomWidth: 1 }}>Today</Text>
                        <View style={{ alignItems: 'center', marginTop: 5 }}>
                            <Text style={{ color: 'blue' }}>Confirmed</Text>
                            <Text style={{ color: 'blue', fontSize: 20 }}>{this.convertNum(state.dataSummaryToday.jumlahKasusBaruperHari)}</Text>
                            <Text style={{ color: 'green' }}>Recovered</Text>
                            <Text style={{ color: 'green', fontSize: 20 }}>{this.convertNum(state.dataSummaryToday.jumlahKasusSembuhperHari)}</Text>
                            <Text style={{ color: 'red' }}>Deaths</Text>
                            <Text style={{ color: 'red', fontSize: 20 }}>{this.convertNum(state.dataSummaryToday.jumlahKasusMeninggalperHari)}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: -20, alignItems: 'center', marginBottom: 5 }}>
                    <Text style={{ fontSize: 20, color: 'white' }}>Cases in {state.dataProv.length} Province</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    {state.dataProv.length > 0 &&
                        <FlatList
                            data={state.dataProv}
                            renderItem={(data) => <ListProv data={data.item} />}
                            keyExtractor={(item) => item.fid}
                            numColumns={2}
                        />
                    }
                </View>
            </View>
        )
    }
}

class ListProv extends React.Component {

    convertNum(num = 0) {
        if (num != NaN) {
            return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        }
        else {
            return 0
        }
    };

    render() {
        const data = this.props.data
        return (
            <View style={{ backgroundColor: '#22D4DF', width: '45%', marginLeft: 10, borderRadius: 10, marginBottom: 10 }}>
                <Text style={{ textAlign: 'center', color: 'white', fontSize: 15, borderBottomColor: 'white', borderBottomWidth: 1 }}>{data.provinsi}</Text>
                <View style={{ alignItems: 'center', marginTop: 5 }}>
                    <Text style={{ color: 'blue' }}>Confirmed</Text>
                    <Text style={{ color: 'blue', fontSize: 20 }}>{this.convertNum(data.kasusPosi)}</Text>
                    <Text style={{ color: 'green' }}>Recovered</Text>
                    <Text style={{ color: 'green', fontSize: 20 }}>{this.convertNum(data.kasusSemb)}</Text>
                    <Text style={{ color: 'red' }}>Deaths</Text>
                    <Text style={{ color: 'red', fontSize: 20 }}>{this.convertNum(data.kasusMeni)}</Text>
                </View>
            </View>
        )
    }
};