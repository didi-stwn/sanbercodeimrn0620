import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import About from './about';
import Detail from './detail';
import Home from './home';
import Login from './login';
import Register from './register';
import Indo from './indo';
import Logout from './logout';
import Tips from './tips';

export default class Index extends React.Component {
    render() {
        const Drawer = createDrawerNavigator();
        const AuthStack = createStackNavigator();

        const StackScreenDetail = () => (
            <AuthStack.Navigator initialRouteName="Home">
                <AuthStack.Screen
                    name='Home'
                    component={Home}
                    options={{
                        headerShown: false
                    }}
                />
                <AuthStack.Screen
                    name='Detail'
                    component={Detail}
                    options={{
                        headerShown: false
                    }}
                />
            </AuthStack.Navigator>
        )

        const DrawerScreen = () => (
            <Drawer.Navigator initialRouteName="Home">
                <Drawer.Screen
                    name="Home"
                    component={StackScreenDetail}
                    options={{
                        headerShown: false
                    }}
                />
                <Drawer.Screen
                    name="Corona in Indonesia"
                    component={Indo}
                    options={{
                        headerShown: false
                    }}
                />
                <Drawer.Screen
                    name="Tips Pencegahan Corona"
                    component={Tips}
                    options={{
                        headerShown: false
                    }}
                />
                <Drawer.Screen
                    name="About"
                    component={About}
                    options={{
                        headerShown: false
                    }}
                />
                <Drawer.Screen
                    name="Log Out"
                    component={Logout}
                    options={{
                        headerShown: false
                    }}
                />
            </Drawer.Navigator>
        )

        const StackScreen = () => (
            <AuthStack.Navigator initialRouteName="Login">
                <AuthStack.Screen
                    name='Login'
                    component={Login}
                    options={{
                        headerShown: false
                    }}
                />
                <AuthStack.Screen
                    name='Register'
                    component={Register}
                    options={{
                        headerShown: false
                    }}
                />
                <AuthStack.Screen
                    name='Home'
                    component={DrawerScreen}
                    options={{
                        headerShown: false
                    }}
                />
            </AuthStack.Navigator>
        )

        return (
            <NavigationContainer>
                <StackScreen />
            </NavigationContainer>
        )
    }
}