import React from 'react';
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import store from './Redux/store'

export default class Tips extends React.Component {
    openDrawer() {
        this.props.navigation.openDrawer();
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#45A7DE' }}>
                <View style={{ paddingBottom: 10, marginTop: 40, flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: 'white', borderBottomWidth: 1 }}>
                    <TouchableOpacity onPress={() => this.openDrawer()}>
                        <Icon name="align-justify" style={{ fontSize: 30, color: 'white', marginLeft: 10 }} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', marginRight: 10 }}>
                            <Text style={{ color: 'white' }}>Welcome,</Text>
                            <Text style={{ color: 'white' }}>{store.getState().name}</Text>
                        </View>
                        <View>
                            <Icon name="user-circle" style={{ fontSize: 30, color: 'white', marginRight: 10 }} />
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: 20, marginTop: 20 }}>
                    <Text style={{ width: 40, backgroundColor: 'white', height: 40, borderRadius: 20, fontSize: 20, textAlign: 'center', textAlignVertical: 'center', color: '#45A7DE' }}>1</Text>
                    <Text style={{ marginLeft: 10, fontSize: 16, color: 'white', textAlignVertical: 'center', width: 280 }}>Tetaplah Jaga Kesehatan dan Kebugaran</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: 20, marginTop: 20 }}>
                    <Text style={{ width: 40, backgroundColor: 'white', height: 40, borderRadius: 20, fontSize: 20, textAlign: 'center', textAlignVertical: 'center', color: '#45A7DE' }}>2</Text>
                    <Text style={{ marginLeft: 10, fontSize: 16, color: 'white', textAlignVertical: 'center', width: 280 }}>Mencuci Tangan Sesering Mungkin</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: 20, marginTop: 20 }}>
                    <Text style={{ width: 40, backgroundColor: 'white', height: 40, borderRadius: 20, fontSize: 20, textAlign: 'center', textAlignVertical: 'center', color: '#45A7DE' }}>3</Text>
                    <Text style={{ marginLeft: 10, fontSize: 16, color: 'white', textAlignVertical: 'center', width: 280 }}>Menerapkan Selalu Etika Batuk dan Bersin</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: 20, marginTop: 20 }}>
                    <Text style={{ width: 40, backgroundColor: 'white', height: 40, borderRadius: 20, fontSize: 20, textAlign: 'center', textAlignVertical: 'center', color: '#45A7DE' }}>4</Text>
                    <Text style={{ marginLeft: 10, fontSize: 16, color: 'white', textAlignVertical: 'center', width: 280 }}>Jaga Jarak Saat Bertemu dengan Orang Lain, Minimal Sejauh 1 Meter</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: 20, marginTop: 20 }}>
                    <Text style={{ width: 40, backgroundColor: 'white', height: 40, borderRadius: 20, fontSize: 20, textAlign: 'center', textAlignVertical: 'center', color: '#45A7DE' }}>5</Text>
                    <Text style={{ marginLeft: 10, fontSize: 16, color: 'white', textAlignVertical: 'center', width: 280 }}>Hindari Kerumunan atau Tetaplah di Rumah</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: 20, marginTop: 20 }}>
                    <Text style={{ width: 40, backgroundColor: 'white', height: 40, borderRadius: 20, fontSize: 20, textAlign: 'center', textAlignVertical: 'center', color: '#45A7DE' }}>6</Text>
                    <Text style={{ marginLeft: 10, fontSize: 16, color: 'white', textAlignVertical: 'center', width: 280 }}>Gunakan Masker Apabila Sedang Pergi Keluar Rumah</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginLeft: 20, marginTop: 20 }}>
                    <Text style={{ width: 40, backgroundColor: 'white', height: 40, borderRadius: 20, fontSize: 20, textAlign: 'center', textAlignVertical: 'center', color: '#45A7DE' }}>7</Text>
                    <Text style={{ marginLeft: 10, fontSize: 16, color: 'white', textAlignVertical: 'center', width: 280 }}>Hindari Menyentuh Mata, Hidung, dan Mulut Sebelum Mencuci Tangan</Text>
                </View>
            </View>
        )
    }
}