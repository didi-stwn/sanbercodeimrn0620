import React, { Component } from 'react';
import { Provider } from 'react-redux';

import Index from './components/index';
import store from './components/Redux/store';


export default class TugasNavigation extends Component {
    render() {
        return (
            <Provider store={store}>
                <Index />
            </Provider>
        );
    }
}
