import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import Logo from '../images/logo.png';

export default class Tugas12 extends Component {
    render() {
        return (
            <ScrollView style={styles.container1}>
                <View style={styles.logo}>
                    <Image source={Logo}></Image>
                </View>
                <View style={styles.container}>
                    <View style={styles.login}>
                        <Text style={styles.textLogin}>Login</Text>
                    </View>
                    <View style={styles.form}>
                        <Text style={styles.label}>Username / Email</Text>
                        <TextInput style={styles.input} />
                        <Text style={styles.label}>Password</Text>
                        <TextInput style={styles.input} />
                    </View>
                    <View style={styles.button}>
                        <TouchableOpacity style={styles.login} onPress={() => this.props.navigation.navigate("DrawerScreen")}>
                            <Text style={styles.masuk}>Masuk</Text>
                        </TouchableOpacity>
                        <Text style={styles.atau}>atau</Text>
                        <TouchableOpacity style={styles.login} onPress={() => this.props.navigation.navigate("RegisterScreen")}>
                            <Text style={styles.daftar}>Daftar ?</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container1: {
        flex: 1,
        backgroundColor: 'white'
    },
    logo: {
        marginTop: 20
    },
    container: {
        marginTop: 50,
        marginBottom: 50,
    },
    login: {
        flexDirection: 'row',
        justifyContent: 'center',
        height: 40
    },
    textLogin: {
        color: '#003366',
        fontWeight: '900',
        fontSize: 25
    },
    form: {
        padding: 30,
    },
    label: {
        marginTop: 10,
        color: '#003366',
    },
    input: {
        borderWidth: 1,
        height: 40
    },
    button: {
        justifyContent: 'center',
    },
    masuk: {
        width: 120,
        borderRadius: 20,
        fontSize: 20,
        backgroundColor: '#3EC6FF',
        color: 'white',
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    atau: {
        color: '#3EC6FF',
        padding: 10,
        textAlign: 'center'
    },
    daftar: {
        width: 120,
        borderRadius: 20,
        fontSize: 20,
        backgroundColor: '#003366',
        color: 'white',
        textAlign: 'center',
        textAlignVertical: 'center'
    },
});