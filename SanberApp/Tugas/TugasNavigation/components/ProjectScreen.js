import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';


export default class Project extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>Halaman Project</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        flex:1,
        backgroundColor: 'white'
    },
})