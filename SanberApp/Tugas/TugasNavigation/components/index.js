import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import About from './AboutScreen';
import Add from './AddScreen';
import Login from './LoginScreen';
import Project from './ProjectScreen';
import Register from './RegisterScreen';
import Skill from './SkillScreen';

export default class App extends React.Component {
    render() {
        const Drawer = createDrawerNavigator();
        const Tabs = createBottomTabNavigator();
        const AuthStack = createStackNavigator();

        const AboutStack = createStackNavigator();
        const AddStack = createStackNavigator();
        const LoginStack = createStackNavigator();
        const ProjectStack = createStackNavigator();
        const RegisterStack = createStackNavigator();
        const SkillStack = createStackNavigator();

        const AboutStackScreen = () => (
            <AboutStack.Navigator>
                <AboutStack.Screen name="About" component={About} />
            </AboutStack.Navigator>
        )

        const AddStackScreen = () => (
            <AddStack.Navigator>
                <AddStack.Screen name="Add" component={Add} />
            </AddStack.Navigator>
        )

        const ProjectStackScreen = () => (
            <ProjectStack.Navigator>
                <ProjectStack.Screen name="Project" component={Project} />
            </ProjectStack.Navigator>
        )

        const SkillStackScreen = () => (
            <SkillStack.Navigator>
                <SkillStack.Screen name="Skill" component={Skill} />
            </SkillStack.Navigator>
        )

        const TabsScreen = () => (
            <Tabs.Navigator initialRouteName="Skill">
                <Tabs.Screen name="Skill" component={SkillStackScreen} />
                <Tabs.Screen name="Project" component={ProjectStackScreen} />
                <Tabs.Screen name="Add" component={AddStackScreen} />
            </Tabs.Navigator>
        )

        const DrawerScreen = () => (
            <Drawer.Navigator initialRouteName="Home">
                <Drawer.Screen name="Home" component={TabsScreen} />
                <Drawer.Screen name="About" component={AboutStackScreen} />
            </Drawer.Navigator>
        )

        const StackScreen = () => (
            <AuthStack.Navigator>
                <AuthStack.Screen
                    name='Login'
                    component={Login}
                    options={{ title: "Login" }}
                />
                <AuthStack.Screen
                    name='RegisterScreen'
                    component={Register}
                    options={{ title: "Register" }}
                />
                <AuthStack.Screen
                    name="DrawerScreen"
                    component={DrawerScreen}
                />
            </AuthStack.Navigator>
        )

        return (
            <NavigationContainer>
                <StackScreen />
            </NavigationContainer>
        )
    }
}