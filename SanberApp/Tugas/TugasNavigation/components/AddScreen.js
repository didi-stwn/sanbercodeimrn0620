import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';


export default class Add extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>Halaman Tambah</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        flex:1,
        backgroundColor: 'white'
    },
})