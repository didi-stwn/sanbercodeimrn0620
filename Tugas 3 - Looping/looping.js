// SOAL 1
console.log("SOAL 1")
var i = 1
console.log("LOOPING PERTAMA")
while (i < 21) {
    if (i % 2 === 0) {
        console.log(i + ' - I love coding')
    }
    i++
}
// sekarang i = 21
console.log("LOOPING KEDUA")
while (i > 0) {
    if (i % 2 === 0) {
        console.log(i + ' - I will become a mobile developer')
    }
    i--
}
console.log()




// SOAL 2
console.log("SOAL 2")
console.log("OUTPUT")
for (i = 1; i < 21; i++) {
    if ((i % 3 === 0) && (i % 2 !== 0)) {
        console.log(i + ' - I Love Coding')
    }
    else if (i % 2 !== 0) {
        console.log(i + ' - Santai')
    }
    else if (i % 2 === 0) {
        console.log(i + ' - Berkualitas')
    }
}
console.log()




// SOAL 3
console.log("SOAL 3")
for (i = 0; i < 4; i++) {
    console.log('########')
}
console.log()




// SOAL 4
console.log("SOAL 4")
var j = 0
var pagar = ''
for (i = 1; i < 8; i++) {
    for (j = 0; j < i; j++) {
        pagar = pagar + '#'
    }
    console.log(pagar)
    pagar = ''
}
console.log()




// SOAL 5
console.log("SOAL 5")
for (i = 0; i < 8; i++) {
    if (i % 2 === 0) {
        console.log(' # # # #')
    }
    else {
        console.log('# # # #')
    }
}