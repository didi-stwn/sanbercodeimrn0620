// SOAL 1 ANIMAL CLASS
console.log("========== SOAL 1 ANIMAL CLASS ==========")

class Animal {
    constructor(name) {
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }

    get name() {
        return this._name;
    }

    get legs() {
        return this._legs;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }
}

class Ape extends Animal {
    constructor(_name, _cold_blooded) {
        super(_name, _cold_blooded);
        this._legs = 2;
    }

    yell() {
        return `Auooo`;
    }
}

class Frog extends Animal {
    constructor(_name, _legs, _cold_blooded) {
        super(_name, _legs, _cold_blooded);
    }

    jump() {
        return `hop hop`;
    }
}

var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"

var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop" 

console.log()
console.log()







// SOAL 2 FUNCTION TO CLASS
console.log("========== SOAL 2 FUNCTION TO CLASS ==========")

// FUNCTION BAWAAN
// function Clock({ template }) {
//     var timer;

//     function render() {
//         var date = new Date();

//         var hours = date.getHours();
//         if (hours < 10) hours = '0' + hours;

//         var mins = date.getMinutes();
//         if (mins < 10) mins = '0' + mins;

//         var secs = date.getSeconds();
//         if (secs < 10) secs = '0' + secs;

//         var output = template
//             .replace('h', hours)
//             .replace('m', mins)
//             .replace('s', secs);

//         console.log(output);
//     }

//     this.stop = function () {
//         clearInterval(timer);
//     };

//     this.start = function () {
//         render();
//         timer = setInterval(render, 1000);
//     };

// }

// var clock = new Clock({ template: 'h:m:s' });
// clock.start();

//Function to Class
class Clock {
    constructor({ template }) {
        this.template = template
        this.timer = ''
    }

    static render(temp) {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = temp
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    };

    start() {
        Clock.render(this.template)
        this.timer = setInterval(() => Clock.render(this.template), 1000);
    };
}

var clock = new Clock({ template: 'h:m:s' });
clock.start()