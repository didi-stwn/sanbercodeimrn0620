var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

function start(index, waktu) {
    if (index < books.length) {
        readBooks(waktu, books[index], function (sisa) {
            if (waktu !== sisa) {
                start(index + 1, sisa)
            }
            else {
                console.log(`kurang waktu ${Math.abs(sisa)}`)
            }
        })
    }
}

start(0, 10000)
