var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

function start(index, times) {
    if (index < books.length) {
        readBooksPromise(times, books[index])
            .then(function (sisa) {
                start(index + 1, sisa)
            })
            .catch(function (sisa) {
                console.log(`kurang waktu ${Math.abs(sisa)}`)
            })
    }
}

start(0, 10000)